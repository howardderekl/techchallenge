﻿using System.ComponentModel.DataAnnotations;

namespace TechChallenge.Data.Models
{
    public class Person
    {
        public int PersonId { get; set; }

        [Required, Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Initial")]
        [StringLength(1, ErrorMessage = "Please specify middle initial only")]
        public string MiddleInitial { get; set; }

        [Required, Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required, Display(Name = "Email Address")]
        [EmailAddress(ErrorMessage = "Email is invalid")]
        public string EmailAddress { get; set; }
    }
}